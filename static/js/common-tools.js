var DataPool={
    db_uris: []
};
function db_meta_load(){
    $.post("/bopress/api/code_generation", bo_xsrf({"action":"dsn_get"}), function(rsp){
        if(rsp.data!=""){
            var db_cnn_names_sel = $("#db_cnn_names");
            db_cnn_names_sel.empty();
            var k=0;
            for(var k in rsp.data){
                if(k==0){
                    db_cnn_names_sel.append('<option value="'+rsp.data[k]+'" selected>'+k+'</option>');
                }else{
                    db_cnn_names_sel.append('<option value="'+rsp.data[k]+'">'+k+'</option>');
                }
                k++;
            }
            var cnn_name = db_cnn_names_sel.val();
            $("#dsn_info").html(cnn_name);
            tables_load(cnn_name);
        }else{
            var db_cnn_names_sel = $("#db_cnn_names");
            db_cnn_names_sel.empty();
        }
    }, "json");
}
function tables_load(dsn){
    $.post("/bopress/api/code_generation", bo_xsrf({"action":"tables", "cnn_name": dsn}), function(rsp){
                var db_tables_sel = $("#db_tables");
                db_tables_sel.empty();
                if(rsp.success&&rsp.data!=""){
                    for(var k=0;k<rsp.data.length;k++){
                        db_tables_sel.append('<option value="'+rsp.data[k]+'">'+rsp.data[k]+'</option>');
                    }
                    var v = db_tables_sel.val();
                    table_columns_load(v);
                }
            }, "json")
}
function table_columns_load(table_name){
    $("#exclude_columns_panel").empty();
        $("#search_columns_panel").empty();
        $("#order_columns_panel").empty();
        var v = table_name;
        $("#current_table_name").val(v);
        $.post("/bopress/api/code_generation", bo_xsrf({"action":"columns", "table_name":v}), function(rsp){
        console.log(rsp);
            if(rsp.success==true){
                for(var k=0;k<rsp.data.length;k++){
                    var text_v = rsp.data[k][0];
                    var text_label = rsp.data[k][0]+" (<span style='color:red;'>"+rsp.data[k][1]+"</span>)";
                    $("#exclude_columns_panel").append('<div class="list-group-item"><input type="checkbox" class="flat-red" name="exclude_columns" value="'+text_v+'"/> '+text_label+'</div>');
                    $("#search_columns_panel").append('<div class="list-group-item"><input type="checkbox" class="flat-red" name="search_columns" value="'+text_v+'"/> '+text_label+'</div>');
                    $("#order_columns_panel").append('<div class="list-group-item"><input type="checkbox" class="flat-red" name="order_columns" value="'+text_v+'"/> '+text_label+'</div>');
                    $("#freeze_columns_panel").append('<div class="list-group-item"><input type="checkbox" class="flat-red" name="freeze_columns" value="'+text_v+'"/> '+text_label+'</div>');
                }
                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                  checkboxClass: 'icheckbox_minimal-blue',
                  radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                  checkboxClass: 'icheckbox_minimal-red',
                  radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass: 'iradio_flat-green'
                });
            }
        }, "json");
}
$(function(){
    //$(".select2").select2();
    db_meta_load();
    $('pre codex').each(function(i, block) {
        hljs.highlightBlock(block);
    });
    $("#db_type").change(function(){
        var v = $(this).val();
        if(v=="sqlite"){
            $(".no-file-db").prop("readonly", true);
        }else{
            $(".no-file-db").prop("readonly", false);
        }
    });
    $("#save_db_conn_info_btn").click(function(){
        var d = $("#db_conn_form").serialize();
        $.post("/bopress/api/code_generation", d, function(rsp){
            db_meta_load();
        }, "json");
    });
    $("#db_tables").change(function(){
        var v = $(this).val();
        table_columns_load(v);
    });
    $("#db_cnn_names").change(function(){
        var v = $(this).val();
        $("#dsn_info").html(v);
        tables_load(v);
    });
    $("#remove_dsn_btn").click(function(){
        $.post("/bopress/api/code_generation", bo_xsrf({"action":"dsn_del", "db_conn_name":$("#db_cnn_names").find("option:selected").text()}), function(rsp){
            db_meta_load();
        });
    });
});