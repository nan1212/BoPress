from cx_Freeze import setup, Executable

from bopress import __version__

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os", "wtforms", "wtforms_alchemy", "sqlalchemy", "tornado", "pymysql", "pymssql"],
                     "zip_include_packages": "bopress"}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

# use sqlite, need copy sqlite3.dll to build2exe dir.
setup(name="BoPress",
      version=__version__,
      description="BoPress Admin",
      options={"build_exe": build_exe_options},
      executables=[Executable("bopress.py", base=base, targetName='BoPress.exe', icon='static/favicon.ico')])
