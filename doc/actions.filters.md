当前系统只安置了有限的几个动作和过滤器，动作与过滤器的安置请使用`do_action`、`apply_filters`两个函数，这两个函数位于`hook`模块内，Tornado自带模板也内置了这两个函数。
# 动作 #
> `bo_settings` 全局配置

    def s(settings):
		pass
	
	add_action('bo_settings', s)

> `bo_init` 初始化

    def init():
		pass
	
	add_action('bo_init', init)

> `bo_tornado_server` Tornado服务器启动前

    def start(app):
		pass
	
	add_action('bo_tornado_server', start)

> `bo_api_(httpmethod)_(apiname)` Ajax动作

    def save(handler):
		pass
	
	add_action('bo_api_post_save_post', save)

> `bo_urlmapping` 自定义handler映射
    
    from tornado.web import RequestHandler, url
    
    class IndexHandler(RequestHandler):
        pass
        
    def url_mapping(urls):
		urls.append(url(r'/', IndexHandler))
	
	add_action('bo_urlmapping', url_mapping)
	
# 过滤器 #

> `bo_dashboard` 仪表盘面板

    def dashboard(htmls, current_screen):
        htmls.append('<div><input name="1"/></div>')
		return htmls
	
	add_filter('bo_dashboard', dashboard)
	
> `bo_options_general_form` 选项表单

    def form(form, current_screen):
		if current_screen.id=='domain-any-setting':
			form='<input name="1"/>'
		return form
	
	add_filter('bo_options_general_form', form)

> `bo_options_general_name` 选项组名称

    def name(group_name, current_screen):
		if current_screen.id=='domain-any-setting':
			group_name='aaa'
		return group_name
	
	add_filter('bo_options_general_name', form)

> `bo_bo_user_login_header` 登陆页header

    def filter(htmls, current_screen):
        htmls.append('<div><input name="1"/></div>')
		return htmls
	
	add_filter('bo_user_login_header', filter)

> `bo_user_login_footer` 登陆页footer

    def filter(htmls, current_screen):
        htmls.append('<div><input name="1"/></div>')
		return htmls
	
	add_filter('bo_user_login_footer', filter)
	
> `bo_user_loginform` 登陆页表单

    def filter(htmls, current_screen):
        htmls.append('<div><input name="1"/></div>')
		return htmls
	
	add_filter('bo_user_loginform', filter)
	
> `bo_user_loginform_footer` 登陆页表单footer

    def filter(htmls, current_screen):
        htmls.append('<div><input name="1"/></div>')
		return htmls
	
	add_filter('bo_user_loginform_footer', filter)
----------

附加插件，代码自动生成，具体可查看自定代码生成模板章节。
> `bo_code_generation_properties_form` 代码生成插件参数表单

    def form(form, current_screen):
		if current_screen.id=='code-html-forms':
			form='<html/>'
		return form
	
	add_filter('bo_code_generation_properties_form', form)

> `bo_code_generation_buttons` 代码生成按钮动作

    def buttons(btns, current_screen):
	    if current_screen.id == "code-jquery-datatables":
	        btns.append(("DataTable", "code/jquery-datatables.tpl"))
	    return btns
	
	add_filter('bo_code_generation_buttons', buttons)